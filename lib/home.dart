// import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // static const duration = const Duration(milliseconds: 1000);
  static const platform = MethodChannel("com.example.stopwatch");
  int secondsPassed = 0;
  bool isActive = false;

  playTimer() async {
    try{
      secondsPassed = await platform.invokeMethod("play");
    }
    on PlatformException catch(ex) {
      print(ex.message);
    }
  }
  pauseTimer() async {
    try{
      secondsPassed = await platform.invokeMethod("pause");
    }
    on PlatformException catch(ex) {
      print(ex.message);
    }
  }
  resetTimer() async {
    try{
      secondsPassed = await platform.invokeMethod("reset");
    }
    on PlatformException catch(ex) {
      print(ex.message);
    }
  }

  // Timer timer;

  // void handleTick() {
  //   if (isActive) {
  //     setState(
  //       () {
  //         secondsPassed = secondsPassed + 1;
  //         // didChangeAppLifecycleState();
  //       },
  //     );
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // if (timer == null) {
    //   timer = Timer.periodic(
    //     duration,
    //     (Timer t) {
    //       handleTick();
    //     },
    //   );
    // }

    int seconds = secondsPassed % 60;
    int minutes = secondsPassed ~/ 60;
    minutes %= 60;
    int hourse = secondsPassed ~/ (60 * 60);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      color: Colors.white,
      home: Scaffold(
        // appBar: AppBar(
        //   leading: new IconButton(
        //       icon: new Icon(Icons.arrow_back),
        //       onPressed: () async {
        //         Navigator.pop(context, widget);
        //       }),
        // ),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.07,
              vertical: MediaQuery.of(context).size.height * 0.07),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              //Title
              Text(
                'Timer',
                style: TextStyle(
                  fontSize: MediaQuery.of(context).size.width * 0.15,
                  fontWeight: FontWeight.w900,
                  color: Color.fromRGBO(49, 68, 105, 1),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height * 0.06),

              //Display
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  LabelText(
                    label: 'HRS',
                    value: hourse.toString().padLeft(2, '0'),
                  ),
                  LabelText(
                    label: 'MIN',
                    value: minutes.toString().padLeft(2, '0'),
                  ),
                  LabelText(
                    label: 'SEC',
                    value: seconds.toString().padLeft(2, '0'),
                  ),
                ],
              ),

              SizedBox(height: MediaQuery.of(context).size.height * 0.05),

              //PlayButoon
              NeumorphicButton(
                boxShape: NeumorphicBoxShape.circle(),
                padding:
                    EdgeInsets.all(MediaQuery.of(context).size.height * 0.1),
                onClick: () {
                  setState(() {
                    isActive = !isActive;
                  });
                  secondsPassed = isActive
                      ? playTimer()
                      : pauseTimer();
                },
                style: NeumorphicStyle(
                  shape: NeumorphicShape.convex,
                  // depth: 8,
                  // intensity: 0.5,
                  // surfaceIntensity: 0.5,
                  color: Colors.teal,
                ),
                child: Icon(
                  isActive ? Icons.pause : Icons.play_arrow,
                  color: Colors.white,
                  size: MediaQuery.of(context).size.height * 0.1,
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height * 0.05),

              //ResetButton
              NeumorphicButton(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.02,
                    horizontal: MediaQuery.of(context).size.width * 0.2),
                onClick: () {
                  setState(() {
                    if (isActive) isActive = false;
                    // secondsPassed = 0;
                  });
                },
                style: NeumorphicStyle(
                  shape: NeumorphicShape.concave,
                  // depth: 8,
                  // intensity: 0.5,
                  // surfaceIntensity: 0.5,
                  color: Colors.white,
                ),
                child: Text(
                  'Reset',
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width * 0.13,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LabelText extends StatelessWidget {
  LabelText({this.label, this.value});

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.07),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: Colors.grey[200],
        boxShadow: [
          BoxShadow(
            color: Colors.grey[400],
            offset: Offset(4, 4),
            blurRadius: 12.0,
            spreadRadius: 2.0,
          ),
          BoxShadow(
            color: Colors.white,
            offset: Offset(-4, -4),
            blurRadius: 12.0,
            spreadRadius: 2.0,
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            '$value',
            style: TextStyle(
              color: Colors.black,
              fontSize: MediaQuery.of(context).size.width * 0.1,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            '$label',
            style: TextStyle(
              color: Colors.black87,
            ),
          ),
        ],
      ),
    );
  }
}
