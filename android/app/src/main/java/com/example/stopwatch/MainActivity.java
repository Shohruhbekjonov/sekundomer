package com.example.stopwatch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "com.example.stopwatch";
    Intent forServiceIntent;
    static int secondsPassed = 0;
    long timeSwapBuff = 0L;


    @Override
    public void configureFlutterEngine(@NonNull @NotNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(Objects.requireNonNull(getFlutterEngine()).getDartExecutor()
                .getBinaryMessenger(), CHANNEL).setMethodCallHandler(
                (call, result) -> {

                    forServiceIntent = new Intent(MainActivity.this, MyService.class);

                    registerReceiver(broadcastReceiver, new IntentFilter(MyService.CHANNEL));

                    startService(forServiceIntent);

                    if (call.method.equals("play")) {
                        startService(forServiceIntent);
                        Log.d("start", String.valueOf(secondsPassed));
                        result.success(secondsPassed);
                    }
                    if (call.method.equals("pause")) {
                        stopService(forServiceIntent);
                        Log.d("pause", String.valueOf(secondsPassed));
                        result.success(secondsPassed);
                    }
                    if (call.method.equals("reset")) {
                        startService(forServiceIntent);
                        Log.d("reset", String.valueOf(secondsPassed));
                        result.success(secondsPassed);
                    }

                });
    }

//    private void startServiceFor() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForegroundService(forServiceIntent);
//        } else {
//        }
//    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateSecond(intent);
        }
    };

    private void updateSecond(Intent intent) {
//        int time =
        if (intent.getExtras() != null) {
            secondsPassed = intent.getIntExtra("countseconds", 0);
//            Log.d("TAG", String.valueOf(secondsPassed));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        registerReceiver(broadcastReceiver, new IntentFilter(MyService.CHANNEL));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, MyService.class));
    }

}