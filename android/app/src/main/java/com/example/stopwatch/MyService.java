package com.example.stopwatch;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import androidx.annotation.Nullable;


public class MyService extends Service {
    public static final String CHANNEL = "com.example.stopwatch";
    private final Intent intent = new Intent(CHANNEL);
    Handler handler = new Handler();
    long timeMilliSecond = 0L;
    private long initial_time;


    @Override
    public void onCreate() {
        super.onCreate();

        initial_time = SystemClock.uptimeMillis();

        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, 1000);  //1 second
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            sendData();
            handler.postDelayed(this, 1000);
        }
    };

    private void sendData() {
        timeMilliSecond = SystemClock.uptimeMillis() - initial_time;

        int timer = (int) timeMilliSecond / 1000;
        intent.putExtra("countseconds", timer);
        sendBroadcast(intent);
//        Log.d("sec", String.valueOf(timer));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}


